package u06lab.code

object TicTacToe extends App {
  sealed trait Player{
    def other: Player = this match {
      case X => O;
      case _ => X
    }

    override def toString: String = this match {
      case X => "X";
      case _ => "O"
    }
  }

  case object X extends Player

  case object O extends Player

  case class Mark(x: Int, y: Int, player: Player)

  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Int, y: Int): Option[Player] = board.find(m => m.x == x && m.y == y).map(_.player)

  def placeAnyMark(board: Board, player: Player): Seq[Board] =
    for (x <- 0 to 2;
         y <- 0 to 2
         if !board.exists(m => m.x == x && m.y == y))
      yield Mark(x, y, player) :: board

  type WinCondition = Board => Iterable[List[Mark]]

  object WinCondition {
    val DIAGONAL: WinCondition = board => List(board.filter(m => m.x == m.y), board.filter(m => m.x == 2 - m.y))
    val COLUMN: WinCondition = board => board.groupBy(_.y).values
    val ROW: WinCondition = board => board.groupBy(_.x).values
  }

  def gameEnd(b: Board): Boolean =
    b.groupBy(_.player)
      .values
      .exists(playerBoard =>
        (WinCondition.DIAGONAL(playerBoard)
          ++ WinCondition.ROW(playerBoard)
          ++ WinCondition.COLUMN(playerBoard))
          .exists(_.size == 3)
      )


  def computeAnyGame(player: Player, moves: Int): Stream[Game] = moves match {
    case 0 => Stream(List(List()))
    case n =>
      for (game <- computeAnyGame(player.other, n - 1);
           nextMove <- placeAnyMark(game.head, player))
        yield nextMove :: game
  }

  def computeAnyGameWithStop(player: Player, moves: Int): Stream[Game] = moves match {
    case 0 => Stream(List(List()))
    case n =>
      for {
        game <- computeAnyGameWithStop(player.other, n - 1)
        nextMove <- placeAnyMark(game.head, player)
      } yield if (!gameEnd(game.head)) nextMove :: game else game
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ".")
      if (x == 2) {
        print(" ")
        if (board == game.head) println()
      }
    }
}