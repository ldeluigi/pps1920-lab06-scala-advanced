package test

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u06lab.code.{Functions, FunctionsImpl}

class TryFunctionsTest {
  val f: Functions = FunctionsImpl

  @Test def testSum(): Unit = {
    assertEquals(60.1, f.sum(List(10.0, 20.0, 30.1)), 0.00000001)
    assertEquals(0.0, f.sum(List()), 0.0000001)
  }

  @Test def testConcat(): Unit = {
    assertEquals("abc", f.concat(Seq("a", "b", "c")))
    assertEquals("", f.concat(Seq()))
  }

  @Test def testMax(): Unit = {

    assertEquals(3, f.max(List(-10, 3, -5, 0)))
    assertEquals(-2147483648, f.max(List()))
  }
}
